<?php

/**
 * @file
 * Contains token_login.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function token_login_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the token_login module.
    case 'help.page.token_login':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Replace the default password based authentication mechanism with one-off login tokens.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_form_alter().
 */
function token_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'user_form') {
    $form['account']['pass']['#access'] = FALSE;
    $form['account']['current_pass']['#access'] = FALSE;
  }
}

/**
 * Implements hook_mail().
 */
function token_login_mail($key, &$message, $params) {
  // Invoking required services.
  $token_service = \Drupal::token();
  $token_login_config = \Drupal::config('token_login.settings');
  $site_name = \Drupal::config('system.site')->get('name');

  $subject_variables = ['%site_name' => $site_name];
  // The token service nees the user parameter to generate the login link.
  $body_variables = ['user' => $params['account']];

  switch ($key) {
    case 'login_link':
      // The callback ensures user specific information will be generated,
      // in this case the login link.
      $token_options = [
        'langcode' => $message['langcode'],
        'callback' => 'user_mail_tokens',
        'clear' => TRUE,
      ];
      $message['subject'] = (string) t('Login link for %site_name', $subject_variables, $token_options);
      $message['body'][] = $token_service->replace($token_login_config->get('message'), $body_variables, $token_options);
      break;
  }

}
